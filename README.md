PHPCodesniffer bundle for Textmate
==================================
Forked from scottkimGit/php-codesniffer-tmbundle

CHANGES
-------
Set PHPCS_PATH Textmate variable to set phpcs path **whitch phpcs**
Set PHPCS_STANDARD Textmate variable to set the standard **phpcs -i**

FIXED
-----
Fixed the correct value of severity column

